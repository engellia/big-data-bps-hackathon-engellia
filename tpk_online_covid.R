library(tidyverse)
library(vroom)
library(lubridate)
library(scales)
library(hrbrthemes)
library(dplyr)

file <- read.csv("data/id_cases_bali.csv")
glimpse(file)
colnames(file)[2] <- "tanggal"
coba_tanggal <- separate(file, col = tanggal,
                         into = c("year","month","day"))
coba_tanggal$month <- as.numeric(coba_tanggal$month)
coba_tanggal$month <- as.character(coba_tanggal$month)
glimpse(coba_tanggal)

coba_tanggal1 <- coba_tanggal %>%
  unite(col = "month", month, day, year, sep = "/")
glimpse(coba_tanggal1)
colnames(coba_tanggal1)[2] <- "tanggal"

cases_covid <- coba_tanggal1

tpk_online <- read.csv("data/train-online_booking_2020.csv")
glimpse(tpk_online)

tpk_online <- tpk_online %>%
  mutate(booked_room = room_total - all_available_room,
         .after = room_total
  )
glimpse(tpk_online)

tpk_online %>%
  group_by(tanggal) %>%
  summarize(sum_kamar = sum(all_available_room, na.rm = TRUE),
            sum_book_kamar = sum(booked_room, na.rm= TRUE))

tpk_online_covid <- left_join(tpk_online, cases_covid, by = "tanggal")
glimpse(tpk_online_covid)
view(tpk_online_covid)


#===========covid=bulanan==========================
covid2020 <- subset(coba_tanggal, year==2020)
view(covid2020)

covid2021 <- subset(coba_tanggal, year==2021)
view(covid2021)

#=======================================================
coba_bulan <- coba_tanggal %>%
  unite(col = "month", month, year, sep = "/")
gabung_covid <-
  coba_bulan %>%
  group_by(month) %>%
  summarize(sum_newcase = sum(newcase, na.rm = TRUE),
            sum_death = sum(death, na.rm= TRUE),
            sum_recovered = sum(recovered, na.rm= TRUE))
view(gabung_covid)

#====================================================
glimpse(covid2020)
covid_bulan_2020 <-
  covid2020 %>%
  group_by(month) %>%
  summarize(sum_newcase = sum(newcase, na.rm = TRUE),
            sum_death = sum(death, na.rm= TRUE),
            sum_recovered = sum(recovered, na.rm= TRUE))
view(covid_bulan_2020)

#====================================================
glimpse(covid2021)
covid_bulan_2021 <-
  covid2021 %>%
  group_by(month) %>%
  summarize(sum_newcase = sum(newcase, na.rm = TRUE),
            sum_death = sum(death, na.rm= TRUE),
            sum_recovered = sum(recovered, na.rm= TRUE))
view(covid_bulan_2021)

#======================================================
write_csv(tpk_online_covid, "data/tpk_online_covid.csv")
write_csv(gabung_covid, "data/gabung_covid_bulan.csv")
write_csv(covid_bulan_2020, "data/covid_bulan_2020.csv")
write_csv(covid_bulan_2021, "data/covid_bulan_2021.csv")
