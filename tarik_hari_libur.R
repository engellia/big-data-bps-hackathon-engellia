# Load packages -----------------------------------------------------------

library(tidyverse)
library(rvest)

# Scrape data from webpage ------------------------------------------------

#' scrape data from www.liburnasional.com

libur2021_url <- "https://www.liburnasional.com/kalender-lengkap-2021/"
libur2021_html <- read_html(libur2021_url)
libur2021_html

#' In order to extract the data, first we need to find the CSS selector in which the information resides. It's essential for us to inspect HTML elements of our target webpage.

libur2021_list <-
  read_html(libur2021_url) %>%
  html_nodes(".libnas-calendar-full-detail") %>%
  html_table()

glimpse(libur2021_list)

# Preprocess data ---------------------------------------------------------

#' We have successfully scraped the data, but it doesn't mean the data is ready for analysis. Let's perform some data cleaning.

libur2021_semiclean <-
  libur2021_list %>%
  bind_rows() %>%
  transmute(
    date = paste(X1, "2021"),
    event = X3
  ) %>%
  separate_rows(
    date,
    sep = " - "
  ) %>%
  separate(
    date,
    into = c("date", "month", "year"),
    sep = "\\s",
    fill = "right"
  ) %>%
  fill(month, year, .direction = "up")

glimpse(libur2021_semiclean)

#' It would be even better if we combine date, month, and year columns into one. Followed by adjusting it's data type.

libur2021 <-
  libur2021_semiclean %>%
  mutate(
    month = recode(
      month,
      "Januari" = "January",
      "Februari" = "February",
      "Maret" = "March",
      "April" = "April",
      "Mei" = "May",
      "Juni" = "June",
      "Juli" = "July",
      "Agustus" = "August",
      "September" = "September",
      "Oktober" = "October",
      "November" = "November",
      "Desember" = "December"
    )
  ) %>%
  unite(
    col = "date",
    date,
    month,
    year,
    sep = "-"
  ) %>%
  mutate(
    date = as.Date(date, format = "%e-%B-%Y")
  ) %>%
  arrange(date)

glimpse(libur2021)

#' A lot of data re-coding, wasn't it? Is there any other way? Of course!

Sys.setlocale("LC_TIME", "id_ID.UTF-8")

libur2021 <-
  libur2021_semiclean %>%
  unite(
    col = "date",
    date,
    month,
    year,
    sep = "-"
  ) %>%
  mutate(
    date = as.Date(date, format = "%e-%B-%Y")
  ) %>%
  arrange(date)

Sys.setlocale("LC_TIME", "en_US.UTF-8")

glimpse(libur2021)

#' However, the approach above might not working flawlessly across different system operation.

# Properly (function)-ing -------------------------------------------------

#' Great! But how if we wanted to perform similar procedure but for a different year? Of course we can re-write our previous code. But is there any better ways? Welcome: function creation in R.

foo <- function(name = "John Doe") {
  res <- runif(1)
  message(name, ", your random number is ", res, "\n~~Current time: ", Sys.time())
  return(res)
}

foo()

#' In our case, we need a function to abstract our previous procedure. Let's call it `get_holidays()` that has single argument `year`.

get_holidays <- function(year) {

  if (length(year) != 1) {
    stop("please specify one year only", call. = FALSE)
    res <- NULL
  }

  if (!inherits(year, c("integer", "character"))) {
    stop("year must be a positive integer of character string", call. = FALSE)
    res <- NULL
  }

  if (nchar(year) != 4) {
    stop("year must be a four digits number", call. = FALSE)
    res <- NULL
  }

  if (as.numeric(year) < 2013 | as.numeric(year) > 2023) {
    stop("only data since 2013 up to 2023 are available", call. = FALSE)
    res <- NULL
  }

  res <-
    str_glue("https://www.liburnasional.com/kalender-lengkap-{year}/") %>%
    read_html() %>%
    html_nodes(".libnas-calendar-full-detail") %>%
    html_table() %>%
    bind_rows() %>%
    transmute(
      date = str_glue("{X1} {year}"),
      event = X3
    ) %>%
    separate_rows(
      date,
      sep = " - "
    ) %>%
    separate(
      date,
      into = c("date", "month", "year"),
      sep = "\\s",
      fill = "right"
    ) %>%
    fill(month, year, .direction = "up") %>%
    mutate(
      month = recode(
        month,
        "Januari" = "January",
        "Februari" = "February",
        "Maret" = "March",
        "April" = "April",
        "Mei" = "May",
        "Juni" = "June",
        "Juli" = "July",
        "Agustus" = "August",
        "September" = "September",
        "Oktober" = "October",
        "November" = "November",
        "Desember" = "December"
      )
    ) %>%
    unite(
      col = "date",
      date,
      month,
      year,
      sep = "-"
    ) %>%
    mutate(
      date = as.Date(date, format = "%e-%B-%Y")
    ) %>%
    arrange(date)

  return(res)
}

# Holidays after holidays -------------------------------------------------

libur_2021 <- get_holidays(year = 2021L)


# Save the data -----------------------------------------------------------

write_csv(libur_2021, "data/libur2021.csv")
write_rds(libur_2021, "data/libur2021.rds")
