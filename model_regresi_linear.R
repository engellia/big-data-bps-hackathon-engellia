library(dplyr) # data manipulation
library(ggplot2) # data viz
library(car)
library(corrplot)
#library(plotly)
library(reshape) #for using melr --> melt(id.vars = "MEDV")

train <- read.csv("data/train.csv")
test <- read.csv("data/test.csv")
sample <- read.csv("data/sample-submission.csv")

head(train)
count(train)
str(train)
summary(train)

head(test)
count(test)
str(test)

head(sample)
str(sample)

#is.na(train)
sum(is.na(train))
#if NA was present, then use
#train_na_omited <- na.omit(train) # this will delete the rows containing NAs
#is.na(test)

#Data Exploration and Visualization
library(psych)
train1 <- train
glimpse(train1)

train1$TPK <- as.integer(train1$TPK)
train1$tpk_online <- as.integer(train1$tpk_online)

train1 %>%
  select(c(TPK,tpk_online,sum_death,sum_newcase,sum_recovered)) %>%
  melt(id.vars = "TPK") %>%
  ggplot(aes(x = value, y = TPK, colour = variable)) + #creates a blank plot space with y-axis MEDV and x-axis value
  geom_point(alpha = 0.7) + #plots all variable outputs into the same graph
  stat_smooth(aes(colour = "black")) + #plots the fit
  facet_wrap(~variable, scales = "free", ncol = 1) + #plots each variable independently with just one column
  labs(x = "", y = "") +
  theme_minimal() #hides grey background

a <- plot(train1$TPK,train1$tpk_online)
b <- plot(train1$TPK,train1$sum_death)
c <- plot(train1$TPK,train1$sum_recovered)
d <- plot(train1$TPK,train1$sum_newcase)

train1 %>%
  select(c(TPK,tpk_online,sum_death,sum_newcase,sum_recovered)) %>%
  boxplot(train1)

model1 <-
(lm(TPK~tpk_online+sum_death+sum_newcase+sum_recovered, data = train1))

model2 <- (lm(log(TPK)~tpk_online+sum_death+sum_newcase+sum_recovered
           +I(tpk_online^2), data = train1))
mintpk<-min(train$tpk_online)
mintpk
model3 <- (lm(TPK~log(tpk_online)+(tpk_online*sum_newcase)+(tpk_online*sum_death),
              data = train1))
model4 <- (lm(TPK~tpk_online, data = train1))
test1 <- test

test1$TPK <- predict(model1,test)
#str(test1)
#View(test1)
train_test <- rbind(train1,test1)
#str(train_test)
model_train_test <- (lm(TPK~tpk_online+sum_death+sum_newcase+sum_recovered,
                        data = train1))
summary(model_train_test)
AIC(model_train_test)

sample1=sample
sample1$TPK <- predict(model4,test)
head(sample1)
write_csv(sample1, "data/sample2021.csv")


ok <- predict(model3,test)
head(ok)
write_csv(sample1, "data/sample2021.csv")
#==================================
average_tpk_tpkonline <-aggregate(TPK~tpk_online, FUN=mean, data=train1)
model15<-lm(TPK~tpk_online,data=average_tpk_tpkonline)
summary(model15)
AIC(models15)

sample0=sample

sample0$TPK <- predict(model5,test)
head(sample0)
write_csv(sample0, "data/sample2021-2.csv")
#=======================================

model6 <- (lm(log(TPK)~log(tpk_online),
              data = train1))

summary(model6)
AIC(model6)
sample2=sample

sample2$TPK <- predict(model6,test)
head(sample2)
write_csv(sample2, "data/sample2021-1.csv")
#############################################################

coba <- read.csv("data/sample2021.csv")
coba1 <- read.csv("data/sample2021-1.csv")
coba2 <- read.csv("data/sample2021-2.csv")

model7=coba$TPK-coba2$TPK
model7

sample4=sample

sample4$TPK <- model7
head(sample4)
write_csv(sample4, "data/sample2021-3.csv")


#============Modelling Lasso Regresion dan XGBoost model=================
install.packages("caret")
library(xgboost)
library(randomForest)
library(psych)
library(glmnet)
library(caret)

#Lasso regression
set.seed(27042018)
my_control <-trainControl(method="cv", number=5)
lassoGrid <- expand.grid(alpha = 1, lambda = seq(0.001,0.1,by = 0.0005))

train2=train1$tpk_online
tpk=train1$TPK
train4 = train1 %>%
  select(c(TPK,tpk_online,sum_death,sum_newcase,sum_recovered))

#=====================
default_glm_mod = train(
  TPK~tpk_online,
  data = train1,
  trControl = trainControl(method = "cv", number = 5),
  method = "glm",
  family = "binomial"
)
default_glm_mod


lasso_mod <- train(x=train1$tpk_online, y=train1$TPK[!is.na(train1$TPK)],
                   method='glmnet', trControl= my_control,
                   tuneGrid=lassoGrid, data=train1)
lasso_mod

LassoPred <- predict(lasso_mod, test)
predictions_lasso <- exp(LassoPred) #need to reverse the log to the real values
head(predictions_lasso)

#==========================

xgb_grid = expand.grid(
  nrounds = 1000,
  eta = c(0.1, 0.05, 0.01),
  max_depth = c(2, 3, 4, 5, 6),
  gamma = 0,
  colsample_bytree=1,
  min_child_weight=c(1, 2, 3, 4 ,5),
  subsample=1
)
label_train <- train4$TPK[!is.na(train4$TPK)]
test1=test%>%
  select(c(tpk_online,sum_death,sum_newcase,sum_recovered))
test1
# put our testing & training data into two seperates Dmatrixs objects
dtrain <- xgb.DMatrix(data = as.matrix(train4), label= label_train)
dtest <- xgb.DMatrix(data = as.matrix(test1))

default_param<-list(
  objective = "reg:linear",
  booster = "gbtree",
  eta=0.05, #default = 0.3
  gamma=0,
  max_depth=3, #default=6
  min_child_weight=4, #default=1
  subsample=1,
  colsample_bytree=1
)

xgbcv <- xgb.cv( params = default_param, data = dtrain, nrounds = 12,
                 nfold = 5, showsd = T, stratified = T, print_every_n = 40,
                 early_stopping_rounds = 10, maximize = F)

xgb_mod <- xgb.train(data = dtrain, params=default_param, nrounds = 12)

XGBpred <- predict(xgb_mod, dtest)
predictions_XGB <- exp(XGBpred) #need to reverse the log to the real values
head(predictions_XGB)





